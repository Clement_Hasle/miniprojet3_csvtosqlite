tableSIV = '''CREATE TABLE siv (
        adresse_titulaire TEXT NOT NULL, 
        nom TEXT NOT NULL,
        prenom TEXt NOT NULL,
        immatriculation TEXT PRIMARY KEY,
        date_immatriculation TEXT NOT NULL,
        vin INTEGER NOT NULL,
        marque TEXT NOT NULL,
        denomination_commerciale TEXT NOT NULL,
        couleur TEXT NOT NULL,
        carrosserie TEXT NOT NULL,
        categorie TEXT NOT NULL,
        cylindre INTEGER NOT NULL,
        energie INTEGER NOT NULL,
        places INTEGER NOT NULL,
        poids INTEGER NOT NULL,
        puissances INTEGER NOT NULL,
        type TEXT NOT NULL,
        variante TEXT NOT NULL,
        version INTEGER NOT NULL
    )'''

UptateSIV = '''UPDATE siv SET adresse_titulaire = :adresse_titulaire, nom = :nom, prenom = :prenom, immatriculation = :immatriculation, 
                        date_immatriculation = :date_immatriculation, vin = :vin, marque = :marque, denomination_commerciale = :denomination_commerciale, 
                        couleur = :couleur, carrosserie = :carrosserie, categorie = :categorie, cylindre = :cylindre, energie = :energie, places = :places, 
                        poids = :poids, puissances = :puissances, type = :type, variante = :variante, version = :version WHERE immatriculation = :immatriculation'''

InsertSIV = '''INSERT INTO siv VALUES (:adresse_titulaire, :nom, :prenom, :immatriculation, 
                :date_immatriculation, :vin, :marque, :denomination_commerciale, 
                :couleur, :carrosserie, :categorie, :cylindre, :energie, :places, 
                :poids, :puissances, :type, :variante, :version)'''


CHEMIN_DE_LA_BDD = "./Base_de_donnee/bdd.sqlite3"
POSITION_IMMAT = "immatriculation"
DELIMITEUR = ';'