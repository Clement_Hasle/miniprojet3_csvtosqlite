import unittest
import os
import csv
import logging
import sqlite3
from datetime import datetime
from StockageSIV import Verif_Exist_Table, Creation_Table, Verif_Enregistrement, Enregistrement_BDD, Traitement_Ligne, Lecteur_CSV, Initialisation_Logging, Verification_CSV
from const.libSIV import tableSIV

class TestStockageSIV(unittest.TestCase):

    def setUp(self):
        Initialisation_Logging()
        #Ecriture d'un fichier csv test
        csvfile = open('testCSV.csv', 'w')
        csvfile.write('')
        csvfile.close()
        with open('testCSV.csv', 'a', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=';')
            writer.writerow(('adresse_titulaire','nom','prenom','immatriculation','date_immatriculation','vin','marque','denomination_commerciale','couleur','carrosserie','categorie','cylindre','energie','places','poids','puissances','type','variante','version'))
            writer.writerow(('3822 Omar Square Suite 257 Port Emily, OK 43251','Smith','Jerome','OVC-568','03/05/2012','9780082351764','Williams Inc','Enhanced well-modulated moderator','LightGoldenRodYellow','45-1743376','34-7904216','3462','37578077','32','3827','110','Inc','92-3625175','79266482'))
            writer.writerow(('974 Byrd Mountains New Jennifer, IL 01612','Olson','Paul','859 GZP','14/06/2019','9781246585674','Ross PLC','Right-sized secondary array','Snow','03-0258606','71-7143342','4837','15895400','9','5870','298','LLC','37-7112501','39890658'))
            writer.writerow(('604 Willis View Suite 279 Hansenview, NY 26033','Gomez','Jose','2-2270C','29/11/1988','9781027382348','Gillespie PLC','Balanced intangible portal','CadetBlue','26-7045508','16-3556230','1245','99868567','33','3504','406','PLC','67-9571998','92859586'))
            writer.writerow(('3822 Omar Square Suite 257 Port Emily, OK 43251','Jhon','Jerome','OVC-568','03/05/2012','9780082351764','Williams Inc','Enhanced well-modulated moderator','LightGoldenRodYellow','45-1743376','34-7904216','3462','37578077','32','3827','110','Inc','92-3625175','79266482'))
            writer.writerow(('3822 Omar Square Suite 257 Port Emily, OK 43251','Smith','Jer--ome','OVC-568','03"/05/2012','9780082351764','Williams Inc','Enhanced well-modulated moderator','LightGoldenRodYellow','45-1743376','34-7904216','3462','37578077','32','3827','110','Inc','92-3625175','79266482'))
        
        self.list_ligne = []
        with open('testCSV.csv') as fichierProSIV:
            reader = csv.DictReader(fichierProSIV, delimiter=';')
            for ligne in reader:
                self.list_ligne.append(ligne)
        #Ecriture d'un fichier sqLite3 test et d'un self.cursor
        self.con = sqlite3.connect('testBDD.sqlite3')
        self.cursor = self.con.cursor()

    def test_Verif_Exist_Table(self):
        self.assertEqual(Verif_Exist_Table(self.cursor), False)
        self.cursor.execute(tableSIV)
        self.con.commit()
        self.assertEqual(Verif_Exist_Table(self.cursor), True)

    def test_Creation_Table(self):
        self.assertEqual(Verif_Exist_Table(self.cursor), False)
        Creation_Table(self.cursor)
        self.con.commit()
        self.assertEqual(Verif_Exist_Table(self.cursor), True)

    def test_Verif_Enregistrement(self):
        Creation_Table(self.cursor)
        for row in self.cursor.execute('''SELECT count(nom) FROM siv '''):
            data = row[0]
        self.assertEqual(data, 0)
        self.cursor.execute('''INSERT INTO siv (adresse_titulaire, nom, prenom, immatriculation, date_immatriculation, vin, marque, denomination_commerciale, couleur, carrosserie, categorie, cylindre, energie, places, poids, puissances, type ,variante, version)
            VALUES ('3822 Omar Square Suite 257 Port Emily, OK 43251','Smith','Jerome','OVC-568','03/05/2012',9780082351764,'Williams Inc','Enhanced well-modulated moderator','LightGoldenRodYellow','45-1743376','34-7904216',3462,37578077,32,3827,110,'Inc','92-3625175',79266482)
            ''')
        self.cursor.execute('''INSERT INTO siv (adresse_titulaire, nom, prenom, immatriculation, date_immatriculation, vin, marque, denomination_commerciale, couleur, carrosserie, categorie, cylindre, energie, places, poids, puissances, type ,variante, version)
            VALUES ('974 Byrd Mountains New Jennifer, IL 01612','Olson','Paul','859 GZP','14/06/2019',9781246585674,'Ross PLC','Right-sized secondary array','Snow','03-0258606','71-7143342',4837,15895400,9,5870,298,'LLC','37-7112501',39890658)
            ''')
        self.cursor.execute('''INSERT INTO siv (adresse_titulaire, nom, prenom, immatriculation, date_immatriculation, vin, marque, denomination_commerciale, couleur, carrosserie, categorie, cylindre, energie, places, poids, puissances, type ,variante, version)
            VALUES ('604 Willis View Suite 279 Hansenview, NY 26033','Gomez','Jose','2-2270C','29/11/1988',9781027382348,'Gillespie PLC','Balanced intangible portal','CadetBlue','26-7045508','16-3556230',1245,99868567,33,3504,406,'PLC','67-9571998',92859586)
            ''')
        self.con.commit()
        for row in self.cursor.execute('''SELECT count(nom) FROM siv '''):
            data = row[0]
        self.assertEqual(data, 3)
        self.assertEqual(Verif_Enregistrement(('OVC-568',), self.cursor), True)
        self.assertEqual(Verif_Enregistrement(('859 GZP',), self.cursor), True)
        self.assertEqual(Verif_Enregistrement(('2-2270C',), self.cursor), True)
        self.assertEqual(Verif_Enregistrement(('PVC-567',), self.cursor), False)
        self.assertEqual(Verif_Enregistrement(('OOPC-8',), self.cursor), False)

    def test_Enregistrement_BDD(self):
        Creation_Table(self.cursor)
        for row in self.cursor.execute('''SELECT count(nom) FROM siv '''):
            data = row[0]
        self.assertEqual(data, 0)
        Enregistrement_BDD(self.list_ligne[0], self.cursor)
        Enregistrement_BDD(self.list_ligne[1], self.cursor)
        Enregistrement_BDD(self.list_ligne[2], self.cursor)
        self.con.commit()
        for row in self.cursor.execute('''SELECT count(nom) FROM siv '''):
            data = row[0]
        self.assertEqual(data, 3)
        for row in self.cursor.execute('''SELECT nom FROM siv WHERE immatriculation = 'OVC-568' '''):
            data = row[0]
        self.assertEqual(data, 'Smith')
        for row in self.cursor.execute('''SELECT prenom FROM siv WHERE marque = 'Ross PLC' '''):
            data = row[0]
        self.assertEqual(data, 'Paul')
        for row in self.cursor.execute('''SELECT adresse_titulaire FROM siv WHERE prenom = 'Jose' '''):
            data = row[0]
        self.assertEqual(data, '604 Willis View Suite 279 Hansenview, NY 26033')

        Enregistrement_BDD(self.list_ligne[3], self.cursor)
        self.con.commit()
        for row in self.cursor.execute('''SELECT nom FROM siv WHERE immatriculation = 'OVC-568' '''):
            data = row[0]
        self.assertEqual(data, 'Jhon')

    def test_Traitement_Ligne(self):
        Creation_Table(self.cursor)
        for row in self.cursor.execute('''SELECT count(nom) FROM siv '''):
            data = row[0]
        self.assertEqual(data, 0)
        Traitement_Ligne(self.list_ligne[0], self.cursor)
        self.con.commit()
        for row in self.cursor.execute('''SELECT count(nom) FROM siv '''):
            data = row[0]
        self.assertEqual(data, 1)
        for row in self.cursor.execute('''SELECT nom FROM siv WHERE immatriculation = 'OVC-568' '''):
            data = row[0]
        self.assertEqual(data, 'Smith')

        Traitement_Ligne(self.list_ligne[3], self.cursor)
        self.con.commit()
        for row in self.cursor.execute('''SELECT nom FROM siv WHERE immatriculation = 'OVC-568' '''):
            data = row[0]
        self.assertEqual(data, 'Jhon')
        
        Traitement_Ligne(self.list_ligne[4], self.cursor)
        self.con.commit()
        for row in self.cursor.execute('''SELECT prenom FROM siv WHERE immatriculation = 'OVC-568' '''):
            data = row[0]
        self.assertEqual(data, 'Jerome')
        for row in self.cursor.execute('''SELECT date_immatriculation FROM siv WHERE immatriculation = 'OVC-568' '''):
            data = row[0]
        self.assertEqual(data, '03/05/2012')
        
    def test_Lecture_CSV(self):
        Creation_Table(self.cursor)
        for row in self.cursor.execute('''SELECT count(nom) FROM siv '''):
            data = row[0]
        self.assertEqual(data, 0)
        Lecteur_CSV('testCSV.csv', self.cursor)
        self.con.commit()
        for row in self.cursor.execute('''SELECT count(nom) FROM siv '''):
            data = row[0]
        self.assertEqual(data, 3)
        for row in self.cursor.execute('''SELECT nom FROM siv WHERE immatriculation = 'OVC-568' '''):
            data = row[0]
        self.assertEqual(data, 'Smith')
        for row in self.cursor.execute('''SELECT prenom FROM siv WHERE marque = 'Ross PLC' '''):
            data = row[0]
        self.assertEqual(data, 'Paul')
        for row in self.cursor.execute('''SELECT adresse_titulaire FROM siv WHERE prenom = 'Jose' '''):
            data = row[0]
        self.assertEqual(data, '604 Willis View Suite 279 Hansenview, NY 26033')

    def test_Initialisation_Logging(self):        
        log_name= './Logs/{:%Y-%m-%d-%H-%M-%S}.log'.format(datetime.now())
        self.assertEqual(os.path.exists(log_name), True)

    def test_Verification_CSV(self):
        #Ecriture d'un fichier csv vide et d'un fichier text test
        csvfile = open('testCSV_Vide.csv', 'x')
        csvfile.close()
        textfile = open('testText.txt', 'x')
        textfile.write('HelloWorld')
        textfile.close()

        self.assertEqual(Verification_CSV('testCSV_Vide.csv'), False)
        self.assertEqual(Verification_CSV('testText.txt'), False)
        self.assertEqual(Verification_CSV('testCSV.csv'), True)
        
        os.remove('testCSV_Vide.csv')
        os.remove('testText.txt')

    def tearDown(self):
        self.con.close()
        os.remove('testCSV.csv')
        os.remove('testBDD.sqlite3')