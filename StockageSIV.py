import csv
import os
import argparse
from datetime import datetime
import sqlite3
import logging

from const.libSIV import tableSIV, CHEMIN_DE_LA_BDD, POSITION_IMMAT, DELIMITEUR, UptateSIV, InsertSIV

def Connexion_BDD():
    """
    Connexion_BDD : fonction permettant de se connecter à la base de donnée sqlite3
    IN  : /
    OUT : L'objet de connexion suite à la connexion à la base de donnée sqlite3
    """
    logging.info("Connexion à la base de donnée")
    return sqlite3.connect(CHEMIN_DE_LA_BDD)


def Verif_Exist_Table(cursor):
    """
    Verif_Exist_Table : fonction permettant de vérifier si la table siv existe dans la base de donnée.
    Si elle n'existe pas, la fonction interceptera une erreur et retournera FALSE. Sinon TRUE.
    IN  : le curseur de la base de donnée
    OUT : un booléen permettant de savoir si la table existe ou non
    """
    logging.info("Vérifiacation de l'existence de la base de donnée")
    try:
        cursor.execute("SELECT immatriculation FROM siv")
    except:
        logging.info("La table siv n'existe pas")
        return False
    return True

def Creation_Table(cursor):
    """
    Creation_Table : fonction permettant de créer la table siv
    IN  : le curseur de la base de donnée
    OUT : /
    """
    logging.info("Création de la table siv")
    cursor.execute(tableSIV)
    logging.info("La table siv a été créée")

def Lecteur_CSV(chemin_Fichier, cursor):
    """
    Lecteur_CSV : fonction permettant de lire le fichier csv passer en argument de l'appel du programme. 
    La fonction permet de traduire les données dans un dictionnaire. Chaque sous dictionnaire est envoyé à la fonction de Traitement_Ligne
    IN  : le chemin du fichier csv et le curseur de la base de donnée
    OUT : /
    """
    logging.info("Lecture du fichier csv")
    with open(chemin_Fichier) as fichierProSIV:
        reader = csv.DictReader(fichierProSIV, delimiter=DELIMITEUR)
        for ligneProSIV in reader:
            Traitement_Ligne(ligneProSIV, cursor)

def Traitement_Ligne(ligne, cursor):
    """
    Traitemen_Ligne : fonction permettant de nettoyer les valeurs de chaque éléments du dictionnaire et ensuite de les envoyer à Enregistrement_BDD
    IN  : le dictionnaire nommé ligne concernant les valeurs à enregistrer dans la BDD. Toujours le curseur de la base de donnée.
    OUT : /
    """
    logging.info("Traitement des lignes...")
    for key, value in ligne.items():
        ligne[key] = ligne[key].replace("--", "").replace('"', "")
    Enregistrement_BDD(ligne, cursor)

def Enregistrement_BDD(ligne, cursor):
    """
    Enregistrement_BDD : fonction permettant d'enregistrer ou de mettre à jour une occurence de la BDD si la fonction Verif_Enregistrement renvoie true.
    IN  : le dictionnaire nettoyé concernant les valeurs à enregistrer dans la BDD. et le curseur de la base de donnée
    OUT : /
    """
    if(Verif_Enregistrement(ligne, cursor)):
        logging.info("Mise à jour d'un enregistrement de la table siv")
        cursor.execute(UptateSIV, ligne)
    else:
        logging.info("Insertion d'un enregistrement de la table siv")
        cursor.execute(InsertSIV, ligne)

def Verif_Enregistrement(ligne, cursor):
    """
    Verif_Enregistrement : fonction permettant de savoir une occurence de la base a déjà une immatriculation pareille que celle passée dans ligne.
    Si tel est le cas, la fonction renverra TRUE. Sinon FALSE.
    IN  : le dictionnaire nettoyé concernant les valeurs à enregistrer dans la BDD. et le curseur de la base de donnée
    OUT : un booléen.
    """
    cursor.execute('SELECT immatriculation FROM siv WHERE immatriculation= :immatriculation', ligne)
    if not cursor.fetchall():
        return False
    return True

def Initialisation_Logging():
    """
    Inititalisation_Logging : fonction permettant d'initialiser les log 
    IN  : /
    OUT : /
    """
    logging.basicConfig(filename= './Logs/{:%Y-%m-%d-%H-%M-%S}.log'.format(datetime.now()), level=logging.INFO, format ='%(asctime)s | %(levelname)s | %(message)s', filemode='w')

def Verification_CSV(chemin_Fic):
    """
    Verification_CSV : fonction permettant de de vérifier si le fichier csv existe et qu'il est non vide.
    IN  : Le chemin du ficher csv
    OUT : un booléen qui indique si le fichier est conforme(TRUE) ou pas(False)
    """
    if(not(os.path.exists(chemin_Fic))):
        logging.warning("Le fichier entré en argument n'est pas conforme")
        print("Le fichier n'existe pas !")
        return False
    if(not(os.path.isfile(chemin_Fic))):
        logging.warning("Le fichier entré en argument n'est pas conforme")
        print("Le fichier potentiel n'en est pas un !")
        return False
    if(os.path.getsize(chemin_Fic)<=10):
        logging.warning("Le fichier entré en argument n'est pas conforme")
        print("Le fichier est vide !")
        return False
    return True


if __name__=="__main__":
    parser = argparse.ArgumentParser(description="This is a useful description")
    parser.add_argument("chemin_Fichier", help="recupere le chemin du fichier")
    args = parser.parse_args()
    logger = Initialisation_Logging()
    bdd = Connexion_BDD()
    cursor = bdd.cursor()
    if(not Verif_Exist_Table(cursor)):
        Creation_Table(cursor)
    bdd.commit()
    if(Verification_CSV(args.chemin_Fichier)):
        Lecteur_CSV(args.chemin_Fichier, cursor)
    else:
        print("Fin du programme")
    bdd.commit()
    logging.info("Fin du programme")